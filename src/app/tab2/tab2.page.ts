import { Component } from '@angular/core';
import { IamportService } from "iamport-ionic4-kcp";

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  constructor(
    private iamport: IamportService
  ) {}


  payment() {
    const param = {
      pay_method : 'card',
      merchant_uid : 'merchant_' + new Date().getTime(),
      name : '주문명:상품결제테스트',
      amount : 100,
      buyer_email : 'iamport@siot.do',
      buyer_name : '구매자이름',
      buyer_tel : '010-1234-5678',
      buyer_addr : '서울특별시 강남구 삼성동',
      buyer_postcode : '123-456',
      app_scheme : 'gntglobal' //플러그인 설치 시 사용한 명령어 "ionic cordova plugin add cordova-plugin-iamport-kcp --variable URL_SCHEME=ionickcp" 의 URL_SCHEME 뒤에 오는 값을 넣으시면 됩니다.
    };
    
    // 아임포트 관리자 페이지 가입 후 발급된 가맹점 식별코드를 사용
    this.iamport.payment("imp80489297", param )
      .then((response)=> {
        if ( response.isSuccess() ) {
            //TODO : 결제성공일 때 처리
        }
      })
      .catch((err)=> {
        alert(err)
      })
    ;
  }
}
