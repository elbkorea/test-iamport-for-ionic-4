import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab2Page } from './tab2.page';

// IamportIonic4KcpModule, IamportService 불러오기
import { IamportIonic4KcpModule } from 'iamport-ionic4-kcp';
import { IamportService } from 'iamport-ionic4-kcp';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    IamportIonic4KcpModule,
    RouterModule.forChild([{ path: '', component: Tab2Page }])
  ],
  declarations: [Tab2Page],
  providers: [IamportService]
})
export class Tab2PageModule {}
